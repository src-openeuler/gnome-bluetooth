Name:           gnome-bluetooth
Epoch:          1
Version:        42.8
Release:        1
Summary:        Bluetooth graphical utilities
License:        GPLv2+ and LGPLv2+
URL:		    https://wiki.gnome.org/Projects/GnomeBluetooth
Source0:	    https://download.gnome.org/sources/gnome-bluetooth/42/gnome-bluetooth-%{version}.tar.xz

BuildRequires:  gettext gobject-introspection-devel gtk3-devel gtk-doc meson pkgconfig(libnotify) systemd-devel
BuildRequires:	pkgconfig(gsound)
BuildRequires:	pkgconfig(upower-glib) >= 0.99.14
BuildRequires:	libadwaita-devel
BuildRequires:	python3-dbusmock >= 0.25.0-1

Requires:       bluez >= 5.0 pulseaudio-module-bluetooth bluez-obexd
Provides:       dbus-bluez-pin-helper gnome-bluetooth-libs = %{epoch}:%{version}-%{release}
Obsoletes:      gnome-bluetooth-libs < %{epoch}:%{version}-%{release}

%description
An application that let you manage Bluetooth in the GNOME destkop.
This package also contains libraries needed for applications that
want to display a Bluetooth device selection widget.

%package        libs-devel
Summary:        Development files for gnome-bluetooth-devel
License:        LGPLv2+
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description    libs-devel
This package contains header files that are needed for writing applications
that require a Bluetooth device selection widget.

%package        help
Summary:        Help documents for gnome-bluetooth

%description    help
The gnome-bluetooth-help package conatins manual pages for gnome-bluetooth.

%prep
%autosetup -p1

%build
%meson -Dgtk_doc=true
%meson_build

%install
%meson_install

%find_lang gnome-bluetooth-3.0

%files -f gnome-bluetooth-3.0.lang
%doc README.md NEWS COPYING COPYING.LIB
%{_bindir}/bluetooth-sendto
%{_datadir}/applications/*.desktop
%{_datadir}/gnome-bluetooth-3.0/
%{_libdir}/libgnome-bluetooth-3.0.so.*
%{_libdir}/libgnome-bluetooth-ui-3.0.so.*
%{_libdir}/girepository-1.0/GnomeBluetooth-3.0.typelib

%files libs-devel
%{_includedir}/gnome-bluetooth-3.0/
%{_libdir}/libgnome-bluetooth-3.0.so
%{_libdir}/libgnome-bluetooth-ui-3.0.so
%{_libdir}/pkgconfig/gnome-bluetooth-3.0.pc
%{_libdir}/pkgconfig/gnome-bluetooth-ui-3.0.pc
%{_datadir}/gir-1.0/GnomeBluetooth-3.0.gir
%{_datadir}/gtk-doc

%files help
%{_mandir}/man1/*

%changelog
* Thu Mar 07 2024 liweigang <izmirvii@gmail.com> - 1:42.8-1
- update to version 42.8

* Fri Nov 24 2023 lwg <liweiganga@uniontech.com> - 1:42.7-1
- update to version 42.7

* Mon Jan 2 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 1:42.5-1
- Upgrade to 42.5

* Thu Jun 23 2022 liukuo <liukuo@kylinos.cn> - 1:42.1-1
- Upgrade to 42.1

* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1:42.0-1
- Upgrade to 42.0

* Fri May 28 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.34.5-1
- Upgrade to 3.34.5
- Update Version, Release, Source0, stage 'files'

* Sat Nov 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.28.2-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add the release number

* Thu Nov 21 2019 liujing<liujing144@huawei.com> - 3.28.2-2
- Package init
